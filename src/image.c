#include <malloc.h>
#include "image.h"

struct image image_create(uint64_t width, uint64_t height) {
    return (struct image) {
            .width = width,
            .height = height,
            .data = malloc(sizeof(struct pixel) * width * height)
    };
}

void image_free(struct image *image) {
    free(image->data);
    image->data = NULL;
}

uint64_t get_src_index(const struct image *source, int32_t c_height, int32_t c_width) {
    return (*source).width * c_height + c_width;
}

uint64_t get_rotated_index(const struct image *source, int32_t c_height, int32_t c_width) {
    return (*source).height * c_width + (
            (*source).height - 1 - c_height);
}

struct image rotate(struct image const source) {
    struct image rotated = image_create(source.height, source.width);
    for (int32_t c_height = 0; c_height < source.height; c_height++) {
        for (int32_t c_width = 0; c_width < source.width; c_width++) {
            rotated.data[get_rotated_index(&source, c_height, c_width)] = source.data[get_src_index(&source, c_height,
                                                                                                    c_width)];
        }
    }
    return rotated;
}
