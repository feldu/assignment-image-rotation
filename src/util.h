#ifndef ASSIGNMENT_IMAGE_ROTATION_UTIL_H
#define ASSIGNMENT_IMAGE_ROTATION_UTIL_H

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_COUNT_OF_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_PADDING
};

static char const *const read_errors_messages[] = {
        [READ_INVALID_SIGNATURE] = "Read error: Invalid signature",
        [READ_INVALID_COUNT_OF_BITS] = "Read error: Invalid bits",
        [READ_INVALID_HEADER] = "Read error: Invalid header",
        [READ_INVALID_PADDING] = "Read error: can't read padding"
};

enum write_status {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_DATA_ERROR,
    WRITE_PADDING_ERROR
};

static char const *const write_errors_messages[] = {
        [WRITE_DATA_ERROR] = "Write error: failed to write data",
        [WRITE_HEADER_ERROR] = "Write error: failed to write header",
        [WRITE_PADDING_ERROR] = "Write error: failed to write padding"
};

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR
};
static char const *const open_errors_messages[] = {
        [OPEN_ERROR] = "Open error",
};

enum save_status {
    SAVE_OK = 0,
    SAVE_ERROR
};

static char const *const save_errors_messages[] = {
        [SAVE_ERROR] = "Save error",
};

_Noreturn void err(const char *msg, ...);

#endif //ASSIGNMENT_IMAGE_ROTATION_UTIL_H
