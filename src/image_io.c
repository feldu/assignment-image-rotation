#include <stdio.h>
#include "image_io.h"

enum open_status read_image(char const *filename, struct image *image, from_image_format from_format) {
    FILE *in = fopen(filename, "rb");
    if (!in) return OPEN_ERROR;
    enum read_status status = from_format(in, image);
    fclose(in);
    if (status != READ_OK) err(read_errors_messages[status]);
    return OPEN_OK;

}

enum save_status save_image(char const *filename, struct image *image, to_image_format to_format) {
    FILE *out = fopen(filename, "wb");
    if (!out) return SAVE_ERROR;
    enum write_status status = to_format(out, image);
    fclose(out);
    if (status != WRITE_OK) err(write_errors_messages[status]);
    return SAVE_OK;
}