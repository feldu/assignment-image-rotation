#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdint.h>
#include <stdio.h>
#include "util.h"

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image image_create(uint64_t width, uint64_t height);

void image_free(struct image *);

/* создаёт копию изображения, которая повёрнута на 90 градусов */
struct image rotate(struct image const source);

#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
