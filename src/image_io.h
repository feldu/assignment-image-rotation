#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_IO_H

#include "image.h"

typedef enum read_status (*from_image_format)(FILE *file, struct image *image);

enum open_status read_image(char const *, struct image *, from_image_format);

typedef enum write_status (*to_image_format)(FILE *file, struct image const *image);

enum save_status save_image(char const *, struct image *, to_image_format);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_IO_H
